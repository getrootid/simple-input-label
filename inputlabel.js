(function( $ )
{
	$.fn.InputLabel = function(text)
	{
	//	var text = arg;
		if(text == "" || text == null)
		{
			//text = this.parent().next("label").text();
			//this.parent().next("label").hide();
			text = $("label[for='" + this.attr("id") + "']").text();
			text = text.trim();
 			$("label[for='" + this.attr("id") + "']").hide();
		}
	
		this.attr("title", text);
		this.addClass("showing-label");

		if( this.val() == "")
			this.val(text);
		
		var elements = $(this);
	
		this.focus(function()
		{	
			$(this).removeClass("showing-label");
			text = $(this).attr("title"); //$("label[for=" + this.id + "]").text();
			val = $(this).val();
			//alert(val + " " + text);
			if(val == text)
			{
				$(this).val("");
			}
		});
		this.blur(function()
		{
			$(this).addClass("showing-label");
			text = $(this).attr("title");
			if($(this).val() == "")
			{
				$(this).val(text);
			}
		});
		
		this.parents("form").submit( function ()
		{
			text = $(elements).attr("title");
			
			if($(elements).val() == text)
			{
				$(elements).val("");
			}
		});
	}
})(jQuery);